package com.code_breaker.openpdf1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.artifex.mupdfdemo.MuPDFActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import java.io.File;
import java.io.IOException;

import file_manager.Activity_files;

public class MainActivity extends AppCompatActivity {

    String namaPackage = "cn.wps.moffice_eng";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_main);


        Button button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isAPPInstalled(namaPackage)) {
                    File file = null;
                    try {
                        file = new File(getApplicationContext().getAssets().open("test.pdf").toString());
                        //Uri uri = ;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    //intent.setDataAndType(Uri.fromFile(file), "application/pdf");
                    intent.setPackage(namaPackage);
                    intent.setData(Uri.fromFile(file));
                    intent.setType("application/pdf");
                    intent.setAction(Intent.ACTION_VIEW);
                    startActivity(intent);
                } else {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=" + namaPackage)));
                }
            }
        });

        Button btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                openFile();
                //String pdf_path = "/sdcard/test.pdf";
                //String pdf_path = "/mnt/sdcard/Download/test2.pdf";
/*
                String pdf_path = "file:///android_asset/test4.pdf";
                File file = new File(pdf_path);
            if( file.exists() ) {
                mPDFManager.show(getApplicationContext(), pdf_path, "");
			}
			else {
                Toast.makeText(getApplicationContext(), getString(R.string.file_not_exist) + pdf_path, Toast.LENGTH_SHORT).show();
			}
*/
            }
        });
    }

    private void openFile() {
        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        Intent i = new Intent(getApplicationContext(), Activity_files.class);
                        startActivity(i);

/*
                        File file = null;
                        file = new File("/storage/emulated/0/Download/Materi google surabaya 18 april 2017.pdf");
                        //Uri uri = ;
                        //Uri uri = Uri.fromFile(pathFile);
                        Intent intent = new Intent(getApplicationContext(), MuPDFActivity.class);
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.setData(Uri.fromFile(file));
                        startActivity(intent);
*/

                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        Toast.makeText(MainActivity.this, "Permission is needed!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    private boolean isAPPInstalled(String namaPackage) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(namaPackage, PackageManager.GET_ACTIVITIES);
            //pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
}
